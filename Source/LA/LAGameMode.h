// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LAGameMode.generated.h"

UCLASS(minimalapi)
class ALAGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALAGameMode();
};



