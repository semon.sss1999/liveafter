// Copyright Epic Games, Inc. All Rights Reserved.

#include "LAGameMode.h"
#include "LACharacter.h"
#include "UObject/ConstructorHelpers.h"

ALAGameMode::ALAGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
